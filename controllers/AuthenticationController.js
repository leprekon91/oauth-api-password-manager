const AuthenticationService = require('../services/AuthenticationService');
const { parseBearer } = require('../utils/parseBearer');

const login = async (req, res) => {
  const { username, password } = req.body;

  // Validate user input
  if (!(username && password)) {
    return res.status(400).send('Username and Password are both required');
  }

  try {
    const { access, refresh } = await AuthenticationService.authenticateUser(
      username,
      password
    );
    return res.json({ access, refresh });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);

    return res.status(401).send('User authentication failed!');
  }
};

const refreshToken = async (req, res) => {
  const currentRefresh = parseBearer(req.headers.authorization);

  if (!currentRefresh) {
    return res.status(400).send('Bearer token is required');
  }

  try {
    const { access, refresh } = await AuthenticationService.refreshAccess(
      currentRefresh
    );

    return res.json({ access, refresh });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);

    return res.status(401).send('Refresh token is invalid');
  }
};

module.exports = { login, refreshToken };
