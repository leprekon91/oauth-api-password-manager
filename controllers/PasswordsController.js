const PasswordsService = require('../services/PasswordsService');

const create = async (req, res) => {
  const { service, password, userId } = req.body;

  try {
    await PasswordsService.createService(service, password, userId);

    return res.status(200).send('Password Created');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);

    return res.status(400).send('Password Service Creation Failed');
  }
};

const read = async (req, res) => {
  const { userId } = req.body;
  const { service: serviceName } = req.params;

  const servicePassword = await PasswordsService.readService(
    serviceName,
    userId
  );

  if (!servicePassword) {
    return res.status(400).send('Password Service Not Found');
  }
  const { service, password } = servicePassword;

  return res.json({ service, password });
};

const update = async (req, res) => {
  const { userId, password } = req.body;
  const { service: serviceName } = req.params;
  try {
    await PasswordsService.updateService(serviceName, password, userId);

    return res.status(200).send('Password Updated');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);

    return res.status(400).send('Password Update Failed');
  }
};

const destroy = async (req, res) => {
  const { userId } = req.body;
  const { service: serviceName } = req.params;

  try {
    await PasswordsService.deleteService(serviceName, userId);

    return res.status(200).send('Password Deleted');
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);

    return res.status(400).send('Password Deletion Failed');
  }
};

module.exports = {
  create,
  read,
  update,
  destroy,
};
