module.exports = {
  parseBearer(header) {
    if (!header) return false;

    const tokenArray = header.split(' ');

    if (tokenArray.length !== 2 || tokenArray[0] !== 'Bearer') {
      return false;
    }

    return tokenArray[1];
  },
};
