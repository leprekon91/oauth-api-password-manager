[![LinkedIn][linkedin-shield]][https://www.linkedin.com/in/andreygrab/]
<br />

<p align="center">
  <h3 align="center">OAuth API Password Manager Example</h3>

  <p align="center">
    An example implementation of OAuth style authentication API. shown here as a password manager use-case.
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a>
      <ul>
        <li><a href="#production">Production</a></li>
        <li><a href="#development">Development</a></li>
        <li><a href="#tests">Tests</a></li>
      </ul>
    </li>
    <li><a href="#contact">Contact</a></li>

  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

### Built With

- Express.js
- Sequelize
- SQLite

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

You will need `NodeJS` and `npm` installed on your machine.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/leprekon91/oauth-api-password-manager.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

<!-- USAGE EXAMPLES -->

## Usage

There are three script commands:

### Production:

```sh
   npm run start
```

### Development:

```sh
   npm run dev
```

### Tests:

```sh
   npm run test
```

## Contact

Andrey Grabarnick - [LinkedIn](https://www.linkedin.com/in/andreygrab/) - reist2009@gmail.com

Project Link: [https://gitlab.com/leprekon91/oauth-api-password-manager](https://gitlab.com/leprekon91/oauth-api-password-manager)
