const { DataTypes, Model } = require('sequelize');
const bcrypt = require('bcrypt');

const { Database } = require('./initDB');

const admin = process.env.ADMIN;
const pass = process.env.PASS;

class User extends Model {
  // validate password hashes
  async validPassword(password) {
    return bcrypt.compare(password, this.password);
  }
}

User.init(
  {
    username: {
      type: DataTypes.TEXT,
      allowNull: false,
      unique: true,
    },
    password: {
      type: DataTypes.STRING(64),
      is: /^[0-9a-f]{64}$/i,
    },
  },
  { sequelize: Database }
);

// Encrypt passwords
User.addHook('beforeCreate', async (user) => {
  // eslint-disable-next-line no-param-reassign
  user.password = await bcrypt.hash(user.password, bcrypt.genSaltSync(8));
});

// Create admin User if none exist
User.count({ where: { username: admin } }).then((count) => {
  if (count === 0) {
    return User.create({ username: admin, password: pass });
  }
  return true;
});

module.exports = User;
