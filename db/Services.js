const { DataTypes, Model } = require('sequelize');

const { Database } = require('./initDB');

class Service extends Model {}

Service.init(
  {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
    },
    service: {
      type: DataTypes.STRING(64),
      is: /^[0-9a-f]{64}$/i,
    },
    password: {
      type: DataTypes.STRING(64),
      is: /^[0-9a-f]{64}$/i,
    },
  },
  { sequelize: Database }
);

module.exports = Service;
