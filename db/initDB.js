const Sequelize = require('sequelize');

const config = {
  PRODUCTION: { dialect: 'sqlite', storage: process.env.DATABASE },
  DEVELOPMENT: { dialect: 'sqlite', storage: process.env.DATABASE },
  TEST: { dialect: 'sqlite', storage: './test.sqlite', logging: false },
}[process.env.NODE_ENV];

const sequelize = new Sequelize(config);

sequelize.sync();

module.exports = { Database: sequelize };
