const { DataTypes, Model, Op } = require('sequelize');

const { Database } = require('./initDB');

class Token extends Model {
  static async validAccess(access) {
    const deadline = new Date().getTime() - 5 * 60 * 1000;

    const token = await Token.findOne({
      where: { access, updatedAt: { [Op.gt]: new Date(deadline) } },
    });

    return token;
  }

  static async validRefresh(refresh) {
    const deadline = new Date().getTime() - 15 * 60 * 1000;

    const token = await Token.findOne({
      where: { refresh, updatedAt: { [Op.gt]: new Date(deadline) } },
    });

    return token;
  }
}

Token.init(
  {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true,
    },
    access: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      unique: true,
    },
    refresh: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      unique: true,
    },
  },
  { sequelize: Database }
);

module.exports = Token;
