const express = require('express');

const { authenticateAccessToken } = require('./middleware');
const AuthenticationController = require('../controllers/AuthenticationController');
const PasswordsController = require('../controllers/PasswordsController');

const router = express.Router();

router.post('/login', AuthenticationController.login);
router.get('/refresh', AuthenticationController.refreshToken);

router.post('/auth', authenticateAccessToken, (req, res) => {
  res.json({ uid: req.body.userId });
});

// Services API
router.post('/service', authenticateAccessToken, PasswordsController.create);

router.get(
  '/service/:service',
  authenticateAccessToken,
  PasswordsController.read
);

router.patch(
  '/service/:service',
  authenticateAccessToken,
  PasswordsController.update
);

router.delete(
  '/service/:service',
  authenticateAccessToken,
  PasswordsController.destroy
);

module.exports = router;
