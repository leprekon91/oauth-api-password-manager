const Token = require('../db/Tokens');
const { parseBearer } = require('../utils/parseBearer');

async function authenticateAccessToken(req, res, next) {
  const access = parseBearer(req.headers.authorization);

  if (!access) {
    return res.status(400).send('Bearer token is required');
  }

  const token = await Token.validAccess(access);

  if (!token) {
    return res
      .status(400)
      .send('The bearer token provided in the request is invalid');
  }
  req.body.userId = token.userId;

  return next();
}

module.exports = { authenticateAccessToken };
