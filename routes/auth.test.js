const uuid4 = require('uuid4');
const supertest = require('supertest');

const app = require('../app');

const request = supertest(app);
let Service, Token;

beforeAll(async () => {
  require('../db/initDB');
  Token = require('../db/Tokens');
  Service = require('../db/Services');
});

describe('Test Routes and Server application ', () => {
  // Setup
  beforeAll(async () => {
    await Token.destroy({ truncate: true });
    await Service.destroy({ truncate: true });
  });

  // Teardown
  afterEach(async () => {
    await Token.destroy({ truncate: true });
    await Service.destroy({ truncate: true });
  });

  test('Gets the test endpoint', async () => {
    return new Promise(async function (resolve) {
      const response = await request.get('/');
      expect(response.status).toBe(200);
      expect(response.text).toBe('Health Check, API is Live');
      resolve();
    });
  });

  test('user can login', async () => {
    return new Promise(async function (resolve) {
      const response = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });
      const { body } = response;

      expect(response.status).toBe(200);
      expect(uuid4.valid(body.access)).toBe(true);
      expect(uuid4.valid(body.refresh)).toBe(true);
      resolve();
    });
  });

  test('user can not login without credentials', async () => {
    return new Promise(async function (resolve) {
      const response = await request.post('/api/login');

      expect(response.status).toBe(400);
      expect(response.text).toBe('Username and Password are both required');
      resolve();
    });
  });

  test('user can not login with wrong credentials', async () => {
    return new Promise(async function (resolve) {
      const response = await request
        .post('/api/login')
        .send({ username: 'minda', password: '654321' });

      expect(response.status).toBe(401);
      expect(response.text).toBe('User authentication failed!');
      resolve();
    });
  });

  test('user can refresh', async () => {
    return new Promise(async function (resolve) {
      const { body } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      const response = await request
        .get('/api/refresh')
        .set('Authorization', `Bearer ${body.refresh}`);

      expect(response.status).toBe(200);
      expect(uuid4.valid(response.body.access)).toBe(true);
      expect(uuid4.valid(response.body.refresh)).toBe(true);
      resolve();
    });
  });

  test('user can not refresh without a token', async () => {
    return new Promise(async function (resolve) {
      const { body } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      const response = await request.get('/api/refresh');

      expect(response.status).toBe(400);
      expect(response.text).toBe('Bearer token is required');
      resolve();
    });
  });

  test('user can not refresh without a valid token', async () => {
    return new Promise(async function (resolve) {
      const { body } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      const response = await request
        .get('/api/refresh')
        .set('Authorization', `Bearer invalid-token`);

      expect(response.status).toBe(401);
      expect(response.text).toBe('Refresh token is invalid');
      resolve();
    });
  });

  test('user can create password service', async () => {
    return new Promise(async function (resolve) {
      const {
        body: { access },
      } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      const response = await request
        .post('/api/service')
        .set('Authorization', `Bearer ${access}`)
        .send({ service: 'test', password: 'testPass' });

      const created = await Service.findOne({ where: { userId: 1 } });

      expect(response.status).toBe(200);
      expect(response.text).toBe('Password Created');
      expect(created).not.toBeNull();
      expect(created.service).toBe('test');
      expect(created.password).toBe('testPass');
      resolve();
    });
  });

  test('user can read password service', async () => {
    return new Promise(async function (resolve) {
      const {
        body: { access },
      } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      await Service.create({
        userId: 1,
        service: 'testRead',
        password: 'testPassword',
      });

      const response = await request
        .get(`/api/service/testRead`)
        .set('Authorization', `Bearer ${access}`);

      expect(response.status).toBe(200);
      expect(response.body).not.toBeNull();

      resolve();
    });
  });

  test('user can update password service', async () => {
    return new Promise(async function (resolve) {
      const {
        body: { access },
      } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      await Service.create({
        userId: 1,
        service: 'testRead',
        password: 'testPassword',
      });

      const response = await request
        .patch(`/api/service/testRead`)
        .set('Authorization', `Bearer ${access}`)
        .send({ password: 'changedPassword' });

      const service = await Service.findOne({ where: { service: 'testRead' } });

      expect(response.status).toBe(200);
      expect(response.body).not.toBeNull();
      expect(service).not.toBeNull();
      expect(service.password).toBe('changedPassword');
      resolve();
    });
  });

  test('user can delete password service', async () => {
    return new Promise(async function (resolve) {
      const {
        body: { access },
      } = await request
        .post('/api/login')
        .send({ username: 'admin', password: '123456' });

      await Service.create({
        userId: 1,
        service: 'testRead',
        password: 'testPassword',
      });

      const response = await request
        .delete(`/api/service/testRead`)
        .set('Authorization', `Bearer ${access}`)
        .send({ password: 'changedPassword' });

      const service = await Service.findOne({ where: { service: 'testRead' } });

      expect(response.status).toBe(200);
      expect(response.body).not.toBeNull();
      expect(service).toBeNull();
      resolve();
    });
  });
});
