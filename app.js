const express = require('express');
const routes = require('./routes');
require('./db/initDB');

const app = express();

app.use(express.json());

// Used for health check
app.get('/', (req, res) => res.send('Health Check, API is Live'));

// API is decalared in routes file
app.use('/api', routes);

module.exports = app;
