const Service = require('../db/Services');

const createService = async (service, password, userId) => {
  const exists = await Service.findOne({ where: { userId, service } });
  if (exists) {
    throw new Error('Service already exists for this user');
  }

  return Service.create({ userId, service, password });
};

const readService = async (service, userId) =>
  Service.findOne({ where: { userId, service } });

const updateService = async (service, password, userId) => {
  const servicePassword = await Service.findOne({ where: { userId, service } });
  if (!servicePassword) {
    throw new Error('Service not found');
  }

  servicePassword.password = password;
  return servicePassword.save();
};

const deleteService = async (service, userId) =>
  Service.destroy({ where: { userId, service } });

module.exports = {
  createService,
  readService,
  updateService,
  deleteService,
};
