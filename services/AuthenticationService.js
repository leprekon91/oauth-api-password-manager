const uuid4 = require('uuid4');

const Token = require('../db/Tokens');
const User = require('../db/Users');

const authenticateUser = (username, password) =>
  User.findOne({ where: { username } }).then(async (user) => {
    if (!user) {
      throw new Error('User Not Found');
    } else if (!(await user.validPassword(password))) {
      throw new Error('Invalid Password');
    }

    await Token.destroy({ where: { userId: user.id } });
    const newToken = await Token.create({
      userId: user.id,
      access: uuid4(),
      refresh: uuid4(),
    });

    return newToken;
  });

const refreshAccess = async (refresh) => {
  const token = await Token.validRefresh(refresh);
  if (!token) {
    throw new Error('Refresh token timed out');
  }

  token.access = uuid4();
  token.refresh = uuid4();
  token.save();
  return token;
};

module.exports = { authenticateUser, refreshAccess };
