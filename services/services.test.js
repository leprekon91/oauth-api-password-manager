const uuid4 = require('uuid4');

let Service, Token, PasswordsService, AuthenticationService;

beforeAll(async () => {
  require('../db/initDB');
  PasswordsService = require('./PasswordsService');
  AuthenticationService = require('./AuthenticationService');
  Token = require('../db/Tokens');
  Service = require('../db/Services');
});

/*
 * Password Service tests
 */
describe('Test the Services DB-Service ', () => {
  // Setup
  beforeAll(async () => {
    await Service.destroy({ truncate: true });
  });

  // Teardown
  afterEach(async () => {
    await Service.destroy({ truncate: true });
  });

  test('password can be created:', async () => {
    return new Promise(async function (resolve) {
      await PasswordsService.createService('createTest', 'createTestPass', 5);

      const created = await Service.findOne({
        where: { service: 'createTest' },
      });

      expect(created.service).toBe('createTest');
      expect(created.password).toBe('createTestPass');
      expect(created.userId).toBe(5);
      resolve();
    });
  });

  test('password can be read:', async () => {
    return new Promise(async function (resolve) {
      await Service.create({
        userId: 5,
        service: 'readTest',
        password: 'readTestPass',
      });

      const result = await PasswordsService.readService('readTest', 5);

      expect(result.service).toBe('readTest');
      expect(result.password).toBe('readTestPass');
      expect(result.userId).toBe(5);
      resolve();
    });
  });

  test('password can be updated:', async () => {
    return new Promise(async function (resolve) {
      await Service.create({
        userId: 5,
        service: 'updateTest',
        password: 'updateTestPass',
      });
      await PasswordsService.updateService('updateTest', 'newPass', 5);

      const result = await Service.findOne({
        where: { service: 'updateTest' },
      });

      expect(result.service).toBe('updateTest');
      expect(result.password).toBe('newPass');
      expect(result.userId).toBe(5);
      resolve();
    });
  });

  test('password can be deleted:', async () => {
    return new Promise(async function (resolve) {
      await Service.create({
        userId: 5,
        service: 'deleteTest',
        password: 'deleteTestPass',
      });

      await PasswordsService.deleteService('deleteTest', 5);

      const result = await Service.findOne({
        where: { service: 'deleteTest' },
      });

      expect(result).toBeNull();
      resolve();
    });
  });

  test("password can't be created twice:", async () => {
    return new Promise(async function (resolve) {
      await PasswordsService.createService('twiceTest', 'twiceTestPass', 5);

      try {
        await PasswordsService.createService('twiceTest', 'pass', 5);
      } catch (error) {
        expect(error.message).toBe('Service already exists for this user');
        resolve();
      }
    });
  });

  test('password can not be updated if it does not exist:', async () => {
    return new Promise(async function (resolve) {
      try {
        await PasswordsService.updateService('updateNullTest', 'pass', 5);
      } catch (error) {
        expect(error.message).toBe('Service not found');
        resolve();
      }
    });
  });
});

describe('Test the Authentication DB-Service', () => {
  // Setup
  beforeAll(async () => {
    await Token.destroy({ truncate: true });
  });

  // Teardown
  afterEach(async () => {
    await Token.destroy({ truncate: true });
  });

  test('user can receive access & refresh token', () => {
    return new Promise(async function (resolve) {
      const { access, refresh } = await AuthenticationService.authenticateUser(
        'admin',
        '123456'
      );

      expect(uuid4.valid(access)).toBe(true);
      expect(uuid4.valid(refresh)).toBe(true);
      resolve();
    });
  });

  test('user can refresh token', () => {
    return new Promise(async function (resolve) {
      const { access, refresh } = await Token.create({
        userId: 1,
        access: uuid4(),
        refresh: uuid4(),
      });

      const { access: newAccess, refresh: newRefresh } =
        await AuthenticationService.refreshAccess(refresh);

      const validAccess = uuid4.valid(newAccess);
      const validRefresh = uuid4.valid(newRefresh);

      expect(access).not.toBe(newAccess);
      expect(refresh).not.toBe(newRefresh);
      expect(validAccess).toBe(true);
      expect(validRefresh).toBe(true);
      resolve();
    });
  });

  test('non-Existing User can not receive tokens', () => {
    return new Promise(async function (resolve) {
      try {
        const token = await AuthenticationService.authenticateUser(
          'test',
          '123456'
        );
      } catch (error) {
        expect(error.message).toBe('User Not Found');

        resolve();
      }
    });
  });

  test('user with wrong password can not receive tokens', () => {
    return new Promise(async function (resolve) {
      try {
        const token = await AuthenticationService.authenticateUser(
          'admin',
          '654321'
        );
      } catch (error) {
        expect(error.message).toBe('Invalid Password');

        resolve();
      }
    });
  });

  test('timed out refresh token cannot be used', () => {
    return new Promise(async function (resolve) {
      const token = await Token.create(
        {
          userId: 1,
          access: uuid4(),
          refresh: uuid4(),
          updatedAt: new Date(new Date().getTime() - 20 * 60 * 1000),
        },
        { silent: true }
      );

      try {
        await AuthenticationService.refreshAccess(token.refresh);
      } catch (error) {
        expect(error.message).toBe('Refresh token timed out');

        resolve();
      }
    });
  });
});
